package me.isaac.simple_design_patterns.c08_factory_method_pattern.operation_factories;

import me.isaac.simple_design_patterns.c08_factory_method_pattern.Operation;

public interface IFactoryMethod {
	public Operation getOperation();
}
