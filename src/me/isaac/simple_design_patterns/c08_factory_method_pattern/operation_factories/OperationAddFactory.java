package me.isaac.simple_design_patterns.c08_factory_method_pattern.operation_factories;


import me.isaac.simple_design_patterns.c08_factory_method_pattern.Operation;
import me.isaac.simple_design_patterns.c08_factory_method_pattern.operations.OperationAdd;

public class OperationAddFactory implements IFactoryMethod {

	@Override
	public Operation getOperation() {
		// TODO Auto-generated method stub
		return new OperationAdd();
	}
	
}
