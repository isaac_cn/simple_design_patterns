package me.isaac.simple_design_patterns.c08_factory_method_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;
import me.isaac.simple_design_patterns.c08_factory_method_pattern.operation_factories.OperationMultiplyFactory;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C08RunCase implements RunCaseBase {
    @Override
    public void run() {
        // TODO Auto-generated method stub
        // 使用简单工厂模式
        //如果我们需要增加一个"幂次方"运算方法，需要修改SimpleOperationFactory类，违反了开放-封闭原则
        Operation operation;
        operation = SimpleOperationFactory.createOperation("+");
        operation.setNumberA(1);
        operation.setNumberB(2);
        System.out.println(operation.getResult());
        // 使用工厂方法模式
        //如果我们需要增加一个"幂次方"运算方法，只需要增加一个OperationPow类和OperationPowFactory类，遵守了开放-封闭原则
        Operation operation2;
        operation2 = new OperationMultiplyFactory().getOperation();
        operation2.setNumberA(3);
        operation2.setNumberB(2);
        System.out.println(operation2.getResult());
    }
}
