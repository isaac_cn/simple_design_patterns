package me.isaac.simple_design_patterns.c08_factory_method_pattern.operations;


import me.isaac.simple_design_patterns.c08_factory_method_pattern.Operation;

public class OperationMinus extends Operation {

	@Override
	public double getResult() {
		// TODO Auto-generated method stub
		return this.getNumberA()-this.getNumberB();
	}
	
}
