package me.isaac.simple_design_patterns.c08_factory_method_pattern;


import me.isaac.simple_design_patterns.c08_factory_method_pattern.operations.OperationAdd;
import me.isaac.simple_design_patterns.c08_factory_method_pattern.operations.OperationDivide;
import me.isaac.simple_design_patterns.c08_factory_method_pattern.operations.OperationMinus;
import me.isaac.simple_design_patterns.c08_factory_method_pattern.operations.OperationMultiply;

public class SimpleOperationFactory {
	private static Operation operation;

	public static Operation createOperation(String operate) {
		switch (operate) {
		case "+":
			operation = new OperationAdd();
			break;
		case "-":
			operation = new OperationMinus();
			break;
		case "*":
			operation = new OperationMultiply();
			break;
		case "/":
			operation = new OperationDivide();
			break;
		default:
			break;
		}
		return operation;
	}
}
