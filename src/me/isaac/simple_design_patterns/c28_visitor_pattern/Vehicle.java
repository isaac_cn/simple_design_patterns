package me.isaac.simple_design_patterns.c28_visitor_pattern;

/**
 * "访问者模式"中的Element，这个必须是稳定的
 * 如果增加Element子类会导致Visitor类（接口）及Visitor子类的大幅变动
 *
 * @author IsaacCn
 * @date 2015/12/11
 */
public abstract class Vehicle {
	public abstract void accept(VehicleWorkImpl work);
}
