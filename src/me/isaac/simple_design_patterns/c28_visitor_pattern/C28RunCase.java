package me.isaac.simple_design_patterns.c28_visitor_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;
import me.isaac.simple_design_patterns.c28_visitor_pattern.elements.DieselVehicle;
import me.isaac.simple_design_patterns.c28_visitor_pattern.elements.GasolineVehicle;
import me.isaac.simple_design_patterns.c28_visitor_pattern.visitors.VehicleWorkEngineStart;
import me.isaac.simple_design_patterns.c28_visitor_pattern.visitors.VehicleWorkFueling;
import me.isaac.simple_design_patterns.c28_visitor_pattern.visitors.VehicleWorkLowTemperStart;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C28RunCase implements RunCaseBase {
    @Override
    public void run() {
        Vehicle dieselVehicle = new DieselVehicle();
        Vehicle gasolineVehicle;
        gasolineVehicle = new GasolineVehicle();

        VehicleWorkImpl workEngineStart = new VehicleWorkEngineStart();
        VehicleWorkImpl workFueling = new VehicleWorkFueling();
        VehicleWorkImpl workLowTemperStart = new VehicleWorkLowTemperStart();

        VehicleTraversal traversal = new VehicleTraversal();
        traversal.attatchVehicle(dieselVehicle);
        traversal.attatchVehicle(gasolineVehicle);

        traversal.acceptWork(workEngineStart);
        traversal.acceptWork(workFueling);
        traversal.acceptWork(workLowTemperStart);
    }
}
