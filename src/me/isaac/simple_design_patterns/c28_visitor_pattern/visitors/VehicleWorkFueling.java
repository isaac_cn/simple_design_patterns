package me.isaac.simple_design_patterns.c28_visitor_pattern.visitors;

import me.isaac.simple_design_patterns.c28_visitor_pattern.VehicleWorkImpl;

/**
 * 给汽车加油
 */
public class VehicleWorkFueling implements VehicleWorkImpl {

	@Override
	public void dieselVehicleWork() {
		// TODO Auto-generated method stub
		System.out.println(">>> 柴油车加油 <<<");
		System.out.println("向柴油车内加入0号柴油；");
	}

	@Override
	public void gasolineVehicleWork() {
		// TODO Auto-generated method stub
		System.out.println(">>> 汽油车加油 <<<");
		System.out.println("向汽油车内加入97号乙醇汽油；");
	}

}

