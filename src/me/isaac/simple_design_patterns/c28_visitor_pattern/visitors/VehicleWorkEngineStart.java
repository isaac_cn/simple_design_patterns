package me.isaac.simple_design_patterns.c28_visitor_pattern.visitors;

import me.isaac.simple_design_patterns.c28_visitor_pattern.VehicleWorkImpl;

/**
 * 正常启动
 */
public class VehicleWorkEngineStart implements VehicleWorkImpl {

	@Override
	public void dieselVehicleWork() {
		// TODO Auto-generated method stub
		System.out.println(">>> 柴油汽车启动 <<<");
		System.out.println("Step1:点火，启动机带动发动机运行；");
		System.out.println("Step2:柴油在气缸中被压燃，做功；");
		System.out.println("Step3:循环往复，柴油车启动成功；");
	}

	@Override
	public void gasolineVehicleWork() {
		// TODO Auto-generated method stub
		System.out.println(">>> 汽油汽车启动 <<<");
		System.out.println("Step1:点火，启动机带动发动机运行；");
		System.out.println("Step2:汽油在气缸被火花塞点燃，做功；");
		System.out.println("Step3:循环往复，汽油车启动成功；");
	}

}
