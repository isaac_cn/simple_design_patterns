package me.isaac.simple_design_patterns.c28_visitor_pattern.visitors;

import me.isaac.simple_design_patterns.c28_visitor_pattern.VehicleWorkImpl;

/**
 * 低温环境下启动
 */
public class VehicleWorkLowTemperStart implements VehicleWorkImpl {

	@Override
	public void dieselVehicleWork() {
		// TODO Auto-generated method stub
		System.out.println(">>> 柴油车低温环境启动 <<<");
		System.out.println("预热...断电...预热...断电...点火...失败...");
		System.out.println("主公，柴油结蜡了，给油箱烤烤火吧...");
	}

	@Override
	public void gasolineVehicleWork() {
		// TODO Auto-generated method stub
		System.out.println(">>> 汽油车低温环境启动 <<<");
		System.out.println("一次点火成功，温度低又能奈我何；");
	}

}
