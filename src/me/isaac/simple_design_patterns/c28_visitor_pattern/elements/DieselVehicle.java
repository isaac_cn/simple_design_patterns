package me.isaac.simple_design_patterns.c28_visitor_pattern.elements;

import me.isaac.simple_design_patterns.c28_visitor_pattern.Vehicle;
import me.isaac.simple_design_patterns.c28_visitor_pattern.VehicleWorkImpl;

/**
 * 柴油车
 */

public class DieselVehicle extends Vehicle {

    @Override
    public void accept(VehicleWorkImpl work) {
        // TODO Auto-generated method stub
        work.dieselVehicleWork();
    }

}

