package me.isaac.simple_design_patterns.c28_visitor_pattern;

/**
 * "访问者模式"中的Visitor，这个类（接口）是可扩展的；
 * 也可以使用抽象类；
 *
 * @author IsaacCn
 * @date 2015/12/11
 */
public interface VehicleWorkImpl {
    void dieselVehicleWork();

    void gasolineVehicleWork();
}

