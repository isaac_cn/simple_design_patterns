package me.isaac.simple_design_patterns.c28_visitor_pattern;

/**
 * 包含了被访问对象的集合，每次访问该类会遍历地访问所有Vehicle对象
 */
import java.util.ArrayList;
import java.util.List;

public class VehicleTraversal {
	private List<Vehicle> vehicles = new ArrayList<Vehicle>();

	public void attatchVehicle(Vehicle vehicle) {
		vehicles.add(vehicle);
	}

	public void detach(Vehicle vehicle) {
		vehicles.remove(vehicle);
	}

	public void acceptWork(VehicleWorkImpl work) {
		for (Vehicle vehicle : vehicles) {
			vehicle.accept(work);
		}
	}
}
