package me.isaac.simple_design_patterns;

import me.isaac.simple_design_patterns.c01_static_factory_method.C01RunCase;
import me.isaac.simple_design_patterns.c06_decorator_pattern.C06RunCase;
import me.isaac.simple_design_patterns.c14_obeserver_pattern.C14RunCase;
import me.isaac.simple_design_patterns.c16_state_pattern.C16RunCase;

public class Main {

    public static void main(String[] args) {
        //run(C01RunCase.class);
        //run(C06RunCase.class);
        //run(C14RunCase.class);
        run(C16RunCase.class);
    }

    private static void run(Class<? extends RunCaseBase> runCase) {
        try {
            RunCaseBase caseObj = runCase.newInstance();
            caseObj.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
