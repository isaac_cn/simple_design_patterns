package me.isaac.simple_design_patterns.c13_builder_pattern;

public class Director {
	public void construct(VehicleBuilder builder) {
		builder.buildEngine();
		builder.buildChassis();
		builder.buildGear();
	}
}
