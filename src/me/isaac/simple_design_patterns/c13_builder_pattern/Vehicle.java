package me.isaac.simple_design_patterns.c13_builder_pattern;

import java.util.ArrayList;
import java.util.List;

public class Vehicle {
	private String name;
	List<String> parts = new ArrayList<String>();

	public Vehicle(String name) {
		this.name = name;
	}

	public void addParts(String part) {
		parts.add(part);
	}

	public void show() {
		System.out.println("Show vehicle " + name);
		for (String part : parts) {
			System.out.println(part);
		}
	}
}
