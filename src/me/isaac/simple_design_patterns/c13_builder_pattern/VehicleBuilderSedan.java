package me.isaac.simple_design_patterns.c13_builder_pattern;

public class VehicleBuilderSedan extends VehicleBuilder {
	Vehicle sedan = new Vehicle("Sedan");

	@Override
	public void buildEngine() {
		// TODO Auto-generated method stub
		sedan.addParts("2.5L汽油发动机");
	}

	@Override
	public void buildChassis() {
		// TODO Auto-generated method stub
		sedan.addParts("承载式车身");
	}

	@Override
	public void buildGear() {
		// TODO Auto-generated method stub
		sedan.addParts("7速DSG变速箱");
	}

	@Override
	public Vehicle getProduct() {
		return sedan;
	}
}