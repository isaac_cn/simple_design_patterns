package me.isaac.simple_design_patterns.c13_builder_pattern;

public class VehicleBuilderSUV extends VehicleBuilder {
	Vehicle suv = new Vehicle("SUV");

	@Override
	public void buildEngine() {
		// TODO Auto-generated method stub
		suv.addParts("3.0T涡轮增压柴油发动机");
	}

	@Override
	public void buildChassis() {
		// TODO Auto-generated method stub
		suv.addParts("非承载式车身");
	}

	@Override
	public void buildGear() {
		// TODO Auto-generated method stub
		suv.addParts("6AT变速箱");
	}

	@Override
	public Vehicle getProduct() {
		// TODO Auto-generated method stub
		return suv;
	}

}
