package me.isaac.simple_design_patterns.c13_builder_pattern;

/**
 * @author IsaacCn
 * @date 2015/12/6
 * @comment 在这个类中，对Vehicle这个Product的建造过程进行了抽象，所有Product的Builder都会继承这个类，
 *			从而保证不会出现忘记装Engine或Gear的情况出现；
 */
public abstract class VehicleBuilder {
	public abstract void buildEngine();
	public abstract void buildChassis();
	public abstract void buildGear();
	public abstract Vehicle getProduct();
}
