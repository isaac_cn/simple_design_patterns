package me.isaac.simple_design_patterns.c13_builder_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C13RunCase implements RunCaseBase {
    @Override
    public void run() {
        Director director = new Director();
        VehicleBuilder suvBuilder = new VehicleBuilderSUV();
        VehicleBuilder sedanBuilder = new VehicleBuilderSedan();
        director.construct(suvBuilder);
        director.construct(sedanBuilder);
        suvBuilder.getProduct().show();
        sedanBuilder.getProduct().show();
    }
}
