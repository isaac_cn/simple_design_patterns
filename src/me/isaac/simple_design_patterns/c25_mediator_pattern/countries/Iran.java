package me.isaac.simple_design_patterns.c25_mediator_pattern.countries;


import me.isaac.simple_design_patterns.c25_mediator_pattern.Country;
import me.isaac.simple_design_patterns.c25_mediator_pattern.InternationalOrganization;

public class Iran extends Country {
	public Iran() {
		this.name = "伊朗";
	}

	@Override
	public void announcePoliticalProblem(String content, InternationalOrganization organization) {
		// TODO Auto-generated method stub
		System.out.println(this.name + "向" + organization.getName() + "发出政治信息，内容：" + content);
		organization.notifyMembers(content);
	}

	@Override
	public void announceFinancialProblem(String content, InternationalOrganization organization) {
		// TODO Auto-generated method stub
		System.out.println(this.name + "向" + organization.getName() + "发出经济信息，内容：" + content);
		organization.notifyMembers(content);
	}

	@Override
	public void receiveMessage(String content, InternationalOrganization organization) {
		// TODO Auto-generated method stub
		System.out.println(this.name + "在" + organization.getName() + "收到信息，内容：" + content);
	}
}

