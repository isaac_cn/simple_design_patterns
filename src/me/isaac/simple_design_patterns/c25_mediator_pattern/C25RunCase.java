package me.isaac.simple_design_patterns.c25_mediator_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;
import me.isaac.simple_design_patterns.c25_mediator_pattern.countries.Iran;
import me.isaac.simple_design_patterns.c25_mediator_pattern.countries.USA;
import me.isaac.simple_design_patterns.c25_mediator_pattern.organizations.IMF;
import me.isaac.simple_design_patterns.c25_mediator_pattern.organizations.UnitedNations;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C25RunCase implements RunCaseBase {
    @Override
    public void run() {
        IMF imf = new IMF();
        UnitedNations un = new UnitedNations();

        Iran iran = new Iran();
        USA usa = new USA();

        un.addMember(usa);
        un.addMember(iran);
        imf.addMember(usa);
        imf.addMember(iran);

        usa.announcePoliticalProblem("要求伊朗拆除核设施", un);
        iran.announceFinancialProblem("将减少石油产出", imf);
    }
}
