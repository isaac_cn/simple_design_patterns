package me.isaac.simple_design_patterns.c25_mediator_pattern;

import java.util.ArrayList;
import java.util.List;

public abstract class InternationalOrganization {
	protected String name;
	private List<Country> members;

	public InternationalOrganization() {
		members = new ArrayList<Country>();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<Country> getMembers() {
		return members;
	}

	public void setMembers(List<Country> members) {
		this.members = members;
	}

	public void addMember(Country country) {
		members.add(country);
	}

	public abstract void notifyMembers(String content);
}
