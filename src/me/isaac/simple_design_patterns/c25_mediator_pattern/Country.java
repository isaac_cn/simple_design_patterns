package me.isaac.simple_design_patterns.c25_mediator_pattern;

public abstract class Country {
	public String name;

	public abstract void announcePoliticalProblem(String content,InternationalOrganization organization);

	public abstract void announceFinancialProblem(String content, InternationalOrganization organization);

	public abstract void receiveMessage(String content, InternationalOrganization organization);
}
