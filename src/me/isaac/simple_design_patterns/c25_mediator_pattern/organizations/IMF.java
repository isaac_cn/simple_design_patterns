package me.isaac.simple_design_patterns.c25_mediator_pattern.organizations;


import me.isaac.simple_design_patterns.c25_mediator_pattern.Country;
import me.isaac.simple_design_patterns.c25_mediator_pattern.InternationalOrganization;

public class IMF extends InternationalOrganization {
	public IMF() {
		this.name = "国际货币基金组织";
	}

	@Override
	public void notifyMembers(String content) {
		// TODO Auto-generated method stub
		for (Country member : this.getMembers()) {
			member.receiveMessage(content, this);
		}
	}
}
