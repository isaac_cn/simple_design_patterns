package me.isaac.simple_design_patterns.c27_interpreter_pattern;

public class PlayContext {
	private String playText;

	public String getPlayText() {
		return playText;
	}

	public void setPlayText(String playText) {
		this.playText = playText;
	}

}
