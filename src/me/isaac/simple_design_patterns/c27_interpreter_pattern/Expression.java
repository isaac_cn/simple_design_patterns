package me.isaac.simple_design_patterns.c27_interpreter_pattern;

public abstract class Expression {
	public void interpret(PlayContext context) {
		// O 2 E 0.5 G 0.5 A 3 E 0.5 G 0.5 D 3 E 0.5 G 0.5 A 0.5 O 3 C 1 O 2 A
		// 0.5 G 1 C 0.5 E 0.5 D 3
		String playText;
		char playKey = ' ';
		char inputChar;
		String playValue = "";
		if (context.getPlayText().length() == 0) {
			return;
		} else {
			playText = context.getPlayText();
			inputChar = playText.charAt(0);
			if (inputChar == ' ') {
				playText = playText.substring(0);
				try {
					execute(playKey, Double.parseDouble(playValue));
				} catch (Exception e) {
					e.printStackTrace();
				}
				playValue = "";// 重置
			} else if (inputChar <= 'Z' && inputChar >= 'A') {// 说明这是一个字母
				playKey = inputChar;
				playText = playText.substring(0);
			} else if ((inputChar <= '9' && inputChar >= '0') || inputChar == '.') {
				playValue.concat(Character.toString(inputChar));
			} else {
			}
		}
	}

	public abstract void execute(char key, double value);
}
