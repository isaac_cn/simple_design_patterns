package me.isaac.simple_design_patterns.c17_adapter_pattern;

/**
 * MissileDF21D代表着我们想要复用的类
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class MissileDF21D {
	private String name;

	public MissileDF21D(String name) {
		this.name = name;
	}

	protected void launchDF21D() {
		System.out.println("DF21D机动发射！");
	}

	protected void attackMovableTarget(String movableTarget) {
		System.out.println("DF21D计算弹道中...");
		System.out.println("DF21D计算弹道完成，开始变轨。");
		System.out.println("正在攻击目标" + movableTarget + "，预计2分钟之后到达。");
	}
}
