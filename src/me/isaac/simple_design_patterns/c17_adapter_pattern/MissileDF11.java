package me.isaac.simple_design_patterns.c17_adapter_pattern;

public class MissileDF11 extends Missile {

	public MissileDF11(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	protected void launch() {
		System.out.println(name + "机动发射！");
	}

	protected void attackStaticTarget(String target) {
		System.out.println("正在攻击固定目标" + target);
	}
}

