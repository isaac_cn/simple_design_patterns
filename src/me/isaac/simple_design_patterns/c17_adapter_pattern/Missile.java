package me.isaac.simple_design_patterns.c17_adapter_pattern;

/**
 * 导弹类，针对固定目标。
 * @author IsaacCn
 * @date 2015/12/8
 */
public abstract class Missile {
	protected String name;

	public Missile(String name) {
		this.name = name;
	}

	protected abstract void launch();

	protected abstract void attackStaticTarget(String target);
}
