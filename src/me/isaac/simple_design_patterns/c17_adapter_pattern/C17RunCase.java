package me.isaac.simple_design_patterns.c17_adapter_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C17RunCase implements RunCaseBase {
    @Override
    public void run() {
        Missile df11 = new MissileDF11("东风11-0001");
        Missile df16 = new MissileDF16("东风16-201");
        Missile df21d = new MovableTargetAdapter("东风21D-32");
        df11.launch();
        df11.attackStaticTarget("固定目标A");
        df16.launch();
        df16.attackStaticTarget("固定目标B");
        df21d.launch();
        df21d.attackStaticTarget("移动目标C");
    }
}
