package me.isaac.simple_design_patterns.c17_adapter_pattern;

/**
 * 适配器，针对移动目标。 复用MissileDF21D类中的方法。
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class MovableTargetAdapter extends Missile {
    protected String movableTarget;
    private MissileDF21D missileDF21D = new MissileDF21D(name);

    public MovableTargetAdapter(String name) {
        super(name);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void launch() {
        // TODO Auto-generated method stub
        missileDF21D.launchDF21D();
    }

    @Override
    protected void attackStaticTarget(String target) {
        // TODO Auto-generated method stub
        missileDF21D.attackMovableTarget(target);
    }

}
