package me.isaac.simple_design_patterns.c17_adapter_pattern;

public class MissileDF16 extends Missile {

	public MissileDF16(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void launch() {
		// TODO Auto-generated method stub
		System.out.println(name + "导弹发射井固定发射！");
	}

	@Override
	protected void attackStaticTarget(String target) {
		// TODO Auto-generated method stub
		System.out.println("正在攻击固定目标" + target);
	}

}
