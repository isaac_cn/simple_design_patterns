package me.isaac.simple_design_patterns.c14_obeserver_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;
import me.isaac.simple_design_patterns.c14_obeserver_pattern.with_event.EventObserver;
import me.isaac.simple_design_patterns.c14_obeserver_pattern.with_event.EventSource;
import me.isaac.simple_design_patterns.c14_obeserver_pattern.without_event.*;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C14RunCase implements RunCaseBase {
    @Override
    public void run() {
        useJavaEvent();
    }
    private void useCustomEvent() {
        Observer tom = new ObserverA("Tom");
        Observer jim = new ObserverA("Jim");
        Observer lucy = new ObserverB("Lucy");
        Observer sue = new ObserverB("Sue");

        PublisherImpl boss = new ConcretePublisher("Boss");
        boss.addObserver(tom);
        boss.addObserver(jim);
        boss.addObserver(sue);
        boss.publish();
    }

    private void useJavaEvent() {
        //Java的Event机制
        EventSource eventSource = new EventSource();
        eventSource.addEventListener(new EventObserver());
        eventSource.notifyEvent("Hello World!");
    }
}
