package me.isaac.simple_design_patterns.c14_obeserver_pattern.without_event;

public class ObserverA extends Observer {

	public ObserverA(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		System.out.println("ObserverA[" + getName() + "]收到信息，采取行动1。");
	}

}
