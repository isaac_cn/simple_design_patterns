package me.isaac.simple_design_patterns.c14_obeserver_pattern.without_event;

public interface PublisherImpl {
	void addObserver(Observer observer);
	void removeObserver(Observer observer);
	void publish();
}
