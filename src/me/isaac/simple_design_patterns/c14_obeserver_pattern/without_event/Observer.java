package me.isaac.simple_design_patterns.c14_obeserver_pattern.without_event;

public abstract class Observer {
	private String name;
	
	public Observer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public abstract void update();
}
