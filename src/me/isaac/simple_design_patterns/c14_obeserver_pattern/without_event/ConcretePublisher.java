package me.isaac.simple_design_patterns.c14_obeserver_pattern.without_event;

import java.util.ArrayList;
import java.util.List;

public class ConcretePublisher implements PublisherImpl {
	private List<Observer> observerList = new ArrayList<Observer>();
	private String name;

	public ConcretePublisher(String name) {
		super();
		this.name = name;
	}

	public void addObserver(Observer observer) {
		observerList.add(observer);
	}

	public void removeObserver(Observer observer) {
		observerList.remove(observer);
	}

	public void publish() {
		System.out.println(name + "发出通知");
		for (Observer o : observerList) {
			o.update();
		}
	}
}
