package me.isaac.simple_design_patterns.c14_obeserver_pattern.with_event;

import java.util.Observable;
import java.util.Observer;

public class EventObserver implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        // 这里写Observer的具体逻辑
        System.out.println("--> Observer update()");
        System.out.println("--> " + o.toString());
        System.out.println("--> " + arg);
    }

}
