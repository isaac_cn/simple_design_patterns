package me.isaac.simple_design_patterns.c14_obeserver_pattern.with_event;

import java.util.Observer;

public class EventSource {
	private EventObserveable observeable=new EventObserveable();
	public void addEventListener(Observer listener){
		observeable.addObserver(listener);
	}
	public void removeEventListener(Observer listener){
		observeable.deleteObserver(listener);
	}
	public void notifyEvent(Object arg){
		observeable.action(arg);
	}
}
