package me.isaac.simple_design_patterns.c14_obeserver_pattern.with_event;

import java.util.Observable;

public class EventObserveable extends Observable {
	public void action(Object arg) {
		super.setChanged();
		super.notifyObservers(arg);//��������Observer��update����
	}
}
