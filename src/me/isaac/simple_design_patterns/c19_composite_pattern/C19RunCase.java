package me.isaac.simple_design_patterns.c19_composite_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C19RunCase implements RunCaseBase {
    @Override
    public void run() {
        Agency root = new Company("总公司");
        Agency bjCompany = new Company("北京分公司");
        Agency shCompany = new Company("上海分公司");

        root.addAgency(bjCompany);
        root.addAgency(shCompany);

        root.addAgency(new HRDepartment("总公司人力资源部"));
        root.addAgency(new FinanceDepartment("总公司财务部"));

        bjCompany.addAgency(new HRDepartment("北京分公司人力资源部"));
        bjCompany.addAgency(new FinanceDepartment("北京分公司财务部"));

        shCompany.addAgency(new HRDepartment("上海分公司人力资源部"));
        shCompany.addAgency(new FinanceDepartment("上海分公司财务部"));

        root.showStructure();
    }
}
