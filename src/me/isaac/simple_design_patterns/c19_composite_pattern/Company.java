package me.isaac.simple_design_patterns.c19_composite_pattern;

import java.util.ArrayList;
import java.util.List;

public class Company extends Agency {
	private List<Agency> agencies;

	public Company(String name) {
		super(name);
		agencies = new ArrayList<Agency>();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addAgency(Agency agency) {
		// TODO Auto-generated method stub
		agencies.add(agency);
	}

	@Override
	public void removeAgency(Agency agency) {
		// TODO Auto-generated method stub
		agencies.remove(agency);
	}

	@Override
	public void showStructure() {
		// TODO Auto-generated method stub
		System.out.println(this.name);
		for (Agency agency : agencies) {
			agency.showStructure();
		}
	}

}
