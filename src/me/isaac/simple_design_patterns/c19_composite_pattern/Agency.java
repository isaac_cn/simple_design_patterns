package me.isaac.simple_design_patterns.c19_composite_pattern;

/**
 * CompositePattern中的Component类，不论是子公司(Company)还是部门(Department)都继承自这个类。
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public abstract class Agency {
	protected String name;

	public Agency(String name) {
		this.name = name;
	}

	public abstract void addAgency(Agency agency);

	public abstract void removeAgency(Agency agency);

	public abstract void showStructure();
}
