package me.isaac.simple_design_patterns.c09_prototype_pattern;

public class WorkExperience implements Cloneable {
	private String company;
	private String duration;
	private String position;

	public WorkExperience(String company, String duration, String position) {
		super();
		this.company = company;
		this.duration = duration;
		this.position = position;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void display() {
		System.out.println("Company: " + company);
		System.out.println("Duration: " + duration);
		System.out.println("Position: " + position);
	}

	public Object Clone() {
		try {
			return this.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
