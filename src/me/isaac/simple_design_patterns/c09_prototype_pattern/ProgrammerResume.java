package me.isaac.simple_design_patterns.c09_prototype_pattern;

public class ProgrammerResume extends Resume {
	private String language;

	public ProgrammerResume(String name, int age, String sex, String country) {
		super(name, age, sex, country);
		// TODO Auto-generated constructor stub
	}

	public ProgrammerResume(String name, int age, String sex, String country, String language) {
		super(name, age, sex, country);
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void display() {
		super.display();
		System.out.println("Language: " + getLanguage());
	}
}
