package me.isaac.simple_design_patterns.c09_prototype_pattern;

public class ComplexResume extends Resume {
	private WorkExperience experience;

	public ComplexResume(String name, int age, String sex, String country) {
		super(name, age, sex, country);
	}

	public ComplexResume(String name, int age, String sex, String country, String company, String duration,
			String position) {
		super(name, age, sex, country);
		experience = new WorkExperience(company, duration, position);
		// experience.setCompany(company);
		// experience.setDuration(duration);
		// experience.setPosition(position);
	}

	public ComplexResume(String name, int age, String sex, String country, WorkExperience experience) {
		super(name, age, sex, country);
		this.experience = experience;
	}

	private ComplexResume(WorkExperience experience) {
		this.experience = experience;
	}

	public WorkExperience getExperience() {
		return experience;
	}

	public void setExperience(WorkExperience experience) {
		this.experience = experience;
	}

	public void setExperience(String company, String duration, String position) {
		experience.setCompany(company);
		experience.setDuration(duration);
		experience.setPosition(position);
		System.out.println("setExperience " + experience.toString());
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		super.display();
		if (experience != null) {
			experience.display();
		}
		System.out.println(this.toString());
		System.out.println(experience.toString());
	}

	@Override
	public Object Clone() {
		// ComplexResume complexResume = new ComplexResume(name, age, sex,
		// country);
		ComplexResume complexResume = null;
		try {
			complexResume = (ComplexResume) this.clone();
			complexResume.setExperience((WorkExperience) experience.Clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return complexResume;
	}

	public Object CloneFault() {
		// TODO Auto-generated method stub
		return super.Clone();
	}
}
