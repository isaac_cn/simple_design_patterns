package me.isaac.simple_design_patterns.c09_prototype_pattern;

public abstract class Resume implements Cloneable {
	protected String name;
	protected int age;
	protected String sex;
	protected String country;

	public Resume() {
	}

	public Resume(String name, int age, String sex, String country) {
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void display() {
		System.out.println("---------------------");
		System.out.println("Name: " + getName());
		System.out.println("Age: " + getAge());
		System.out.println("Sex: " + getSex());
		System.out.println("Country: " + getCountry());
	}

	public Object Clone() {
		try {
			return this.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
