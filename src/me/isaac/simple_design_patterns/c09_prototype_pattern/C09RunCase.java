package me.isaac.simple_design_patterns.c09_prototype_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C09RunCase implements RunCaseBase {
    @Override
    public void run() {
        // 浅复制
        // simpleCpy();
        // 深复制
        complexCpy();
    }

    private static void simpleCpy() {
        // 浅复制
        ProgrammerResume resume1 = new ProgrammerResume("Tom", 22, "Male", "US", "C++");
        resume1.display();
        ProgrammerResume resume2 = (ProgrammerResume) resume1.Clone();
        resume2.setAge(33);
        resume2.setCountry("UK");
        resume2.setName("Jack");
        resume2.display();
        resume1.display();
    }

    private static void complexCpy() {
        // 深复制
        // WorkExperience experience = new WorkExperience("Microsoft",
        // "2009.1-2013.2", "Manager");
        // ComplexResume resume3 = new ComplexResume("Linda", 35, "Female",
        // "US", experience);
        // resume3.display();
        // ComplexResume resume4 = (ComplexResume) resume3.Clone();
        // resume4.display();
        // WorkExperience experience1 = new WorkExperience("Google",
        // "2007.6-2011.2", "Programmer");
        // resume4.setExperience(experience1);
        // System.out.println("仅修改了"+resume4.getName()+"的经历信息");
        // resume3.display();
        // resume4.display();
        // System.out.println("使用另外一种修改方法");
        // resume3.setExperience("Yahoo", "2007.3-2009.10", "CFO");
        // resume3.display();
        // resume4.display();
        ComplexResume resume3 = new ComplexResume("Linda", 35, "Female", "US", "Google", "2007.6-2011.2", "Programmer");
        resume3.display();
        ComplexResume resume4 = (ComplexResume) resume3.Clone();
        resume4.setName("Sue");
        resume4.display();

        resume3.setExperience("Yahoo", "2007.3-2009.10", "CFO");
        resume3.display();
        resume4.display();
    }
}
