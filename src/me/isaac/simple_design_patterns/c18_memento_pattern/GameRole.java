package me.isaac.simple_design_patterns.c18_memento_pattern;

/**
 * “备忘录模式”中的Originator类（发起人）
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class GameRole {
    private int currentHp;
    private int currentAttack;
    private int currentDefence;

    public void initialize() {
        currentHp = 100;
        currentAttack = 50;
        currentDefence = 10;
    }

    public void fight() {
        this.currentHp *= 0.8;
        this.currentAttack *= 0.7;
        this.currentDefence *= 0.6;
        System.out.println("战斗，元气大伤。");
    }

    public void recovery(GameRoleState state) {
        this.currentHp = state.getHp();
        this.currentAttack = state.getAttack();
        this.currentDefence = state.getDefence();
        System.out.println("哇哈哈哈哈，满血复活，我胡汉三又回来了。");
    }

    public GameRoleState saveState() {
        System.out.println("保存当前状态");
        return new GameRoleState(currentHp, currentAttack, currentDefence);
    }

    public void showState() {
        System.out.println("当前状态：");
        System.out.println("HP:" + currentHp + ",攻击力:" + currentAttack + ",防御力:" + currentDefence);
    }
}

