package me.isaac.simple_design_patterns.c18_memento_pattern;

/**
 * “备忘录模式”中的Memento类
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class GameRoleState {
	private int hp;
	private int attack;
	private int defence;

	public GameRoleState(int hp, int attack, int defence) {
		this.hp = hp;
		this.attack = attack;
		this.defence = defence;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefence() {
		return defence;
	}

	public void setDefence(int defence) {
		this.defence = defence;
	}

}
