package me.isaac.simple_design_patterns.c18_memento_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C18RunCase implements RunCaseBase {
    @Override
    public void run() {
        GameRole mariao = new GameRole();
        mariao.initialize();

        GameRoleStateManager stateManager = new GameRoleStateManager();
        stateManager.setState(mariao.saveState());

        mariao.showState();
        mariao.fight();
        mariao.showState();
        mariao.recovery(stateManager.getState());
        mariao.showState();
    }
}
