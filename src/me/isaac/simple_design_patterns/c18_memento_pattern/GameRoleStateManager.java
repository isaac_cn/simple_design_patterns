package me.isaac.simple_design_patterns.c18_memento_pattern;

/**
 * “备忘录模式”中的Caretaker类（管理者）
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class GameRoleStateManager {
	private GameRoleState state;

	public GameRoleState getState() {
		return state;
	}

	public void setState(GameRoleState state) {
		this.state = state;
	}

}
