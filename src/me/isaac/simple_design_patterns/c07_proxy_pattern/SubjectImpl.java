package me.isaac.simple_design_patterns.c07_proxy_pattern;

public interface SubjectImpl {
	public void giveFlowers();
	public void giveChocolates();
	public void giveDolls();
}
