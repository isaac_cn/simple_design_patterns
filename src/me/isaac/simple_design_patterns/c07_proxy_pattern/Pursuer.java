package me.isaac.simple_design_patterns.c07_proxy_pattern;

public class Pursuer implements SubjectImpl {
	Beauty mm;

	public Pursuer(Beauty mm) {
		this.mm = mm;
	}

	@Override
	public void giveFlowers() {
		// TODO Auto-generated method stub
		System.out.println("Give flowers to " + mm.getName());
	}

	@Override
	public void giveChocolates() {
		// TODO Auto-generated method stub
		System.out.println("Give chocolates to " + mm.getName());
	}

	@Override
	public void giveDolls() {
		// TODO Auto-generated method stub
		System.out.println("Give dolls to " + mm.getName());
	}

}
