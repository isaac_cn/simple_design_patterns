package me.isaac.simple_design_patterns.c07_proxy_pattern;

public class Beauty {
	private String name;

	public Beauty(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
