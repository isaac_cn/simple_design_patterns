package me.isaac.simple_design_patterns.c07_proxy_pattern;

public class Proxy implements SubjectImpl{
	Pursuer gg;
	public Proxy(Beauty mm){
		gg=new Pursuer(mm);
	}
	
	@Override
	public void giveFlowers() {
		// TODO Auto-generated method stub
		gg.giveFlowers();
	}

	@Override
	public void giveChocolates() {
		// TODO Auto-generated method stub
		gg.giveChocolates();
	}

	@Override
	public void giveDolls() {
		// TODO Auto-generated method stub
		gg.giveDolls();
	}
	
}
