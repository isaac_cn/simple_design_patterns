package me.isaac.simple_design_patterns.c07_proxy_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C07RunCase implements RunCaseBase {
    @Override
    public void run() {
        Beauty mm = new Beauty("Linda");
        Proxy proxy = new Proxy(mm);

        proxy.giveFlowers();
        proxy.giveChocolates();
        proxy.giveDolls();
    }
}
