package me.isaac.simple_design_patterns.c06_decorator_pattern;

/**
 * 装饰者模式
 * 
 * @author IsaacCn
 *
 */
public class Finery extends Person {
	protected Person component;

	public Finery(String name) {
		super(name);
	}

	public Finery() {
	}

	protected void decorate(Person component) {
		this.component = component;
	}

	public void show() {
		//super.show();
		if (component != null) {
			component.show();
		}
	}
}
