package me.isaac.simple_design_patterns.c06_decorator_pattern;

public class Trousers extends Finery {
	public Trousers() {
	}

	public Trousers(String name) {
		super(name);
	}

	public void show() {
		System.out.println("Trousers");
		super.show();
	}
}
