package me.isaac.simple_design_patterns.c06_decorator_pattern;

public class Sweater extends Finery {
	public Sweater() {
	}

	public Sweater(String name) {
		super(name);
	}

	public void show() {
		System.out.println("Sweater");
		super.show();
	}
}
