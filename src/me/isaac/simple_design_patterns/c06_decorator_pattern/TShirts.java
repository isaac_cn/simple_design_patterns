package me.isaac.simple_design_patterns.c06_decorator_pattern;

public class TShirts extends Finery {
	public TShirts() {
	}

	public TShirts(String name) {
		super(name);
	}

	public void show() {
		System.out.println("TShirts");
		super.show();
	}
}
