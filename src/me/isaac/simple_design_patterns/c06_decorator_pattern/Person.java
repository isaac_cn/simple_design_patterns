package me.isaac.simple_design_patterns.c06_decorator_pattern;

/**
 * ConcreteComponent
 *
 * @author IsaacCn 因为只有一个ConcreteComponent，所以没有抽象Component类。
 */
public class Person {
	private String name;

	public Person() {
	}

	public Person(String name) {
		this.name = name;
	}

	public void show() {
		System.out.println("My name is " + name);
	}
}
