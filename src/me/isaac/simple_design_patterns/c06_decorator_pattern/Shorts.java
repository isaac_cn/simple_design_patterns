package me.isaac.simple_design_patterns.c06_decorator_pattern;

public class Shorts extends Finery {
	public Shorts() {
	}

	public Shorts(String name) {
		super(name);
	}

	public void show() {
		System.out.println("Shorts");
		super.show();
	}
}
