package me.isaac.simple_design_patterns.c06_decorator_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C06RunCase implements RunCaseBase {
    @Override
    public void run() {
        Person superman = new Person("SuperMan");
        TShirts tshirts = new TShirts();
        Shorts shorts = new Shorts();
        tshirts.decorate(superman);
        shorts.decorate(tshirts);
        shorts.show();
        System.out.println("--------------------------------------------");
        Person stranger = new Person("LuRenJia");
        Sweater sweater = new Sweater();
        Trousers trousers = new Trousers();
        sweater.decorate(stranger);
        trousers.decorate(sweater);
        trousers.show();
    }
}
