package me.isaac.simple_design_patterns.c23_command_pattern;

public class CommandBakeBeef extends BarbecueCommand {

	public CommandBakeBeef(BarbecueMaster master) {
		super(master);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execCommand() {
		// TODO Auto-generated method stub
		master.bakeBeef();
	}

}