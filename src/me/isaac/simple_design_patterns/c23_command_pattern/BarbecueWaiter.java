package me.isaac.simple_design_patterns.c23_command_pattern;

import java.util.ArrayList;
import java.util.List;

public class BarbecueWaiter {
	private List<BarbecueCommand> commands = new ArrayList<BarbecueCommand>();

	public void addOrder(BarbecueCommand command) {
		commands.add(command);
		System.out.println("增加订单：" + command.toString());
	}

	public void removeOrder(BarbecueCommand command) {
		commands.remove(command);
		System.out.println("取消订单：" + command.toString());
	}

	public void notifyMaster() {
		for (BarbecueCommand command : commands) {
			command.execCommand();
		}
	}
}
