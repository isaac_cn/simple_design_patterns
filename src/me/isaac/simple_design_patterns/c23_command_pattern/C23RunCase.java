package me.isaac.simple_design_patterns.c23_command_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C23RunCase implements RunCaseBase {
    @Override
    public void run() {
        BarbecueMaster master = new BarbecueMaster();
        BarbecueCommand bakeMuttonCommand = new CommandBakeMutton(master);
        BarbecueCommand bakeWingCommand = new CommandBakeWing(master);
        BarbecueCommand bakeBeefCommand = new CommandBakeBeef(master);
        BarbecueWaiter waiter = new BarbecueWaiter();

        waiter.addOrder(bakeBeefCommand);
        waiter.addOrder(bakeWingCommand);
        waiter.addOrder(bakeMuttonCommand);
        waiter.notifyMaster();
    }
}
