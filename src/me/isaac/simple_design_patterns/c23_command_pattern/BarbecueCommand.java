package me.isaac.simple_design_patterns.c23_command_pattern;

public abstract class BarbecueCommand {
	protected BarbecueMaster master;// 烧烤师傅

	public BarbecueCommand(BarbecueMaster master) {
		this.master = master;
	}

	public abstract void execCommand();
}
