package me.isaac.simple_design_patterns.c23_command_pattern;

public class BarbecueMaster {
	// private String name;
	//
	// public BarbecueMaster(String name) {
	// super();
	// this.name = name;
	// }
	//
	// public String getName() {
	// return name;
	// }
	//
	// public void setName(String name) {
	// this.name = name;
	// }
	// public void barbecue(String type,int num){
	// System.out.println("烧烤师傅"+name+":烤了"+num+"串"+type);
	// }
	// public void cancel(String type,int num){
	// System.out.println("烧烤师傅"+name+":取消了"+num+"串"+type);
	// }
	public void bakeMutton() {
		System.out.println("烤羊肉串");
	}

	public void bakeWing() {
		System.out.println("烤翅中");
	}

	public void bakeBeef() {
		System.out.println("烤肥牛");
	}
}

