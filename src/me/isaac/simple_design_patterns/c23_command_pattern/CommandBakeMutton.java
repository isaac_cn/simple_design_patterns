package me.isaac.simple_design_patterns.c23_command_pattern;

public class CommandBakeMutton extends BarbecueCommand {

	public CommandBakeMutton(BarbecueMaster master) {
		super(master);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execCommand() {
		// TODO Auto-generated method stub
		master.bakeMutton();
	}

}
