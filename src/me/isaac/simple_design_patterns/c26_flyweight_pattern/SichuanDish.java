package me.isaac.simple_design_patterns.c26_flyweight_pattern;

/**
 * 川菜，所以包含了"辣度"这个参数
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class SichuanDish extends Dish {
	protected String hotLevel;// "辣度"

	public SichuanDish(String name) {
		super(name);
		this.hotLevel = "正常";
		// TODO Auto-generated constructor stub
	}

	public SichuanDish(String name, String hotLevel) {
		super(name);
		this.hotLevel = hotLevel;
	}

	@Override
	public String getFullName() {
		// TODO Auto-generated method stub
		// if (hotLevel.isEmpty() || hotLevel == null) {
		// return getName();
		// } else {
		// return hotLevel + "的" + getName();
		// }
		return hotLevel + "的" + getName();
	}

}
