package me.isaac.simple_design_patterns.c26_flyweight_pattern;

/**
 * 泰国菜
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class ThaiDish extends Dish {

	public ThaiDish(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getFullName() {
		// TODO Auto-generated method stub
		return getName();
	}

}
