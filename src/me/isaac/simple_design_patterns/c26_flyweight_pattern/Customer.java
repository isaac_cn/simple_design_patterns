package me.isaac.simple_design_patterns.c26_flyweight_pattern;

/**
 * 客户类，"享元模式"中的Client
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class Customer {
	private String name;

	public Customer(String name) {
		super();
		this.name = name;
	}

	public void orderFood(Dish dish) {
		System.out.println(name + "点了：" + dish.getFullName());
		Kitchen.addDish(dish);
	}
}
