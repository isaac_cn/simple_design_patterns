package me.isaac.simple_design_patterns.c26_flyweight_pattern;

/**
 * 菜品类
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public abstract class Dish {
	private String name;

	public Dish(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public abstract String getFullName();
}

