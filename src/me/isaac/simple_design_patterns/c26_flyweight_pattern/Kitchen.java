package me.isaac.simple_design_patterns.c26_flyweight_pattern;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * "厨房"，相当于"享元模式"中的Factory类
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class Kitchen {
	private static Map<String, Dish> menu = new HashMap<String, Dish>();

	public static void addDish(Dish dish) {
		if (!menu.containsKey(dish.getFullName())) {
			menu.put(dish.getFullName(), dish);
		}
	}

	public static void showDishes() {
		System.out.println(">>>下面是厨房今天需要加工的所有菜品<<<");
		Iterator it = menu.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			System.out.println(menu.get(key).getFullName());
		}
	}
}
