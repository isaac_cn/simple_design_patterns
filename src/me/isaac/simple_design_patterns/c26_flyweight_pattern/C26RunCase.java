package me.isaac.simple_design_patterns.c26_flyweight_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C26RunCase implements RunCaseBase {
    @Override
    public void run() {
        Customer tom = new Customer("汤姆");
        Customer xiaoli = new Customer("小李");
        Customer sue = new Customer("苏");

        tom.orderFood(new SichuanDish("熊猫豆腐", "点点辣"));
        tom.orderFood(new SichuanDish("酸菜鱼"));
        tom.orderFood(new ThaiDish("咖喱鸡块"));/* 啥，这饭店同时提供泰国菜和川菜，什么鬼？ */
        tom.orderFood(new ThaiDish("海鲜汤"));
        tom.orderFood(new SichuanDish("红烧茄子"));

        xiaoli.orderFood(new SichuanDish("麻婆豆腐", "变态辣"));
        xiaoli.orderFood(new SichuanDish("水煮鱼"));
        tom.orderFood(new SichuanDish("红烧茄子"));

        sue.orderFood(new SichuanDish("水煮鱼"));
        sue.orderFood(new ThaiDish("海鲜汤"));
        sue.orderFood(new SichuanDish("熊猫豆腐", "微辣"));
		/* 哎 写得我都饿了 */
        Kitchen.showDishes();
    }
}
