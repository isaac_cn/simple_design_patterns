package me.isaac.simple_design_patterns.c24_chain_of_responsibility_pattern;

/**
 * 雇员类，Request的发起者
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class Employee {
	protected String name;

	public Employee(String name) {
		this.name = name;
	}

	public void sendRequest(Request request, Manager manager) {
		System.out.println(name + "请求 " + request.getType() + " " + request.getNum());
		manager.handleRequest(request);
	}
}

