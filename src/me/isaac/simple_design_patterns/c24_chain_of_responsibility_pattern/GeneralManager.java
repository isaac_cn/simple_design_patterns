package me.isaac.simple_design_patterns.c24_chain_of_responsibility_pattern;

/**
 * 总经理
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class GeneralManager extends Manager {

	public GeneralManager(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleRequest(Request request) {
		// TODO Auto-generated method stub
		if (request.getType().equals("请假")) {
			System.out.println(this.name + "批准了请求");
		} else if (request.getType().equals("加薪") && request.getNum() <= 1000) {
			System.out.println(this.name + "批准了请求");
		} else {
			System.out.println("总经理说洗洗睡吧");
		}
	}

}
