package me.isaac.simple_design_patterns.c24_chain_of_responsibility_pattern;

/**
 * 总监
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class Majordomo extends Manager {

	public Majordomo(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleRequest(Request request) {
		// TODO Auto-generated method stub
		if (request.getType().equals("请假") && request.getNum() <= 4) {
			System.out.println(this.name + "批准了请求");
		} else if (request.getType().equals("加薪") && request.getNum() <= 500) {
			System.out.println(this.name + "批准了请求");
		} else {
			//this.setSuperior(new GeneralManager(name));
			superior.handleRequest(request);
		}
	}

}
