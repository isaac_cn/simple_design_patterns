package me.isaac.simple_design_patterns.c24_chain_of_responsibility_pattern;

/**
 * 普通经理
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class CommonManager extends Manager {

	public CommonManager(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handleRequest(Request request) {
		// TODO Auto-generated method stub
		if (request.getType().equals("请假") && request.getNum() <= 2) {
			System.out.println(this.name + "批准了请求");
		} else {
			//this.setSuperior(new Majordomo(name));
			this.superior.handleRequest(request);
		}
	}
}
