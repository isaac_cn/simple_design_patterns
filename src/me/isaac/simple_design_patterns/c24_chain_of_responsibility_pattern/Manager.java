package me.isaac.simple_design_patterns.c24_chain_of_responsibility_pattern;

/**
 * 管理者类，经理/总监/总经理等都继承自该类
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public abstract class Manager {
    protected String name;
    protected Manager superior;

    public Manager(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSuperior(Manager manager) {
        this.superior = manager;
    }

    public abstract void handleRequest(Request request);

}
