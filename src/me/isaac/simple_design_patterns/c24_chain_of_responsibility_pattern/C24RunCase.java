package me.isaac.simple_design_patterns.c24_chain_of_responsibility_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C24RunCase implements RunCaseBase {
    @Override
    public void run() {
        Employee xiaoli = new Employee("小李");
        Employee xiaobai = new Employee("小白");
        Employee xiaolin = new Employee("小林");

        Manager laowang = new CommonManager("经理老王");
        Manager laoliang = new Majordomo("总监老梁");
        Manager laohe = new GeneralManager("总经理老贺");

        laowang.setSuperior(laoliang);
        laoliang.setSuperior(laohe);

        xiaoli.sendRequest(new Request("请假", 2), laowang);
        xiaoli.sendRequest(new Request("加薪", 2000), laowang);

        xiaobai.sendRequest(new Request("请假", 10), laowang);
        xiaobai.sendRequest(new Request("加薪", 1000), laowang);

        xiaolin.sendRequest(new Request("请假", 3), laowang);
        xiaolin.sendRequest(new Request("加薪", 500), laowang);
    }
}
