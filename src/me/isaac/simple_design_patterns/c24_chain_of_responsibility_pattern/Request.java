package me.isaac.simple_design_patterns.c24_chain_of_responsibility_pattern;

/**
 * 请求
 *
 * @author IsaacCn
 * @date 2015/12/10
 */
public class Request {
	private String type;
	private int num;

	public Request(String type, int num) {
		this.type = type;
		this.num = num;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

}