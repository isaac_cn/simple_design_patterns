package me.isaac.simple_design_patterns.c10_template_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C09RunCase implements RunCaseBase{

	@Override
	public void run() {
		TestPaper paper1=new TestPaper1();
		TestPaper paper2=new TestPaper2();
		paper1.testQuestionA();
		paper1.testQuestionB();
		paper1.testQuestionC();
		paper2.testQuestionA();
		paper2.testQuestionB();
		paper2.testQuestionC();
	}
}
