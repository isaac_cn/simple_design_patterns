package me.isaac.simple_design_patterns.c10_template_pattern;

public abstract class TestPaper {
	public void testQuestionA() {
		System.out.println("QuestionA:********************, The answer is " + answerQuestionA());
	}

	public void testQuestionB() {
		System.out.println("QuestionB:********************, The answer is " + answerQuestionB());
	}

	public void testQuestionC() {
		System.out.println("QuestionC:********************, The answer is " + answerQuestionC());
	}

	protected abstract String answerQuestionA();

	protected abstract String answerQuestionB();

	protected abstract String answerQuestionC();
}
