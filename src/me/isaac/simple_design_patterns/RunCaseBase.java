package me.isaac.simple_design_patterns;

public interface RunCaseBase {
    void run();
}
