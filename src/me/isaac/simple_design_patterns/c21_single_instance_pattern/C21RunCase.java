package me.isaac.simple_design_patterns.c21_single_instance_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C21RunCase implements RunCaseBase {
    @Override
    public void run() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                SingleInstanceClass i3 = SingleInstanceClass.getInstance();
                System.out.println(i3.toString());
            }
        }).run();
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                SingleInstanceClass i4 = SingleInstanceClass.getInstance();
                System.out.println(i4.toString());
            }
        }).run();
    }
}
