package me.isaac.simple_design_patterns.c21_single_instance_pattern;

/**
 * 单实例类
 *
 * @author IsaacCn
 * @date 2015/12/9
 */

public class SingleInstanceClass {
	private static SingleInstanceClass uniqueInstance;
	private String name;

	/* 重要:将构造函数声明为private */
	private SingleInstanceClass() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static SingleInstanceClass getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new SingleInstanceClass();
			return uniqueInstance;
		}
		return uniqueInstance;
	}
}
