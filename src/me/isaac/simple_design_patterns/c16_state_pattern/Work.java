package me.isaac.simple_design_patterns.c16_state_pattern;


import me.isaac.simple_design_patterns.c16_state_pattern.states.NoonState;

/**
 * 状态模式中的Context类
 *
 * @author IsaacCn
 * @date 2015/12/18
 */
public class Work {
	private State state;
	private int hour;

	public Work() {
		state = new NoonState();//默认的State
	}

	public Work(State state) {
		super();
		this.state = state;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public void coding() {
		if (hour <= 24) {
			state.handleWork(this);
		}
	}
}
