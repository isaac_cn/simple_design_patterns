package me.isaac.simple_design_patterns.c16_state_pattern;

import me.isaac.simple_design_patterns.c16_state_pattern.Work;

/**
 * 状态模式中的State类
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public abstract class State {
	public abstract void handleWork(Work work);
}
