package me.isaac.simple_design_patterns.c16_state_pattern.states;

import me.isaac.simple_design_patterns.c16_state_pattern.State;
import me.isaac.simple_design_patterns.c16_state_pattern.Work;

/**
 * 状态模式中的ConcreteState类
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class ForenoonState extends State {

	@Override
	public void handleWork(Work work) {
		// TODO Auto-generated method stub
		if (work.getHour() < 12) {
			System.out.println("上午工作，精力充沛，效率百倍。");
		} else {
			work.setState(new NoonState());
			work.coding();
		}
	}

}
