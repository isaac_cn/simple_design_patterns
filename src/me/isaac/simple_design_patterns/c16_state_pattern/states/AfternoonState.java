package me.isaac.simple_design_patterns.c16_state_pattern.states;

import me.isaac.simple_design_patterns.c16_state_pattern.State;
import me.isaac.simple_design_patterns.c16_state_pattern.Work;

/**
 * 状态模式中的ConcreteState类
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class AfternoonState extends State {

	@Override
	public void handleWork(Work work) {
		// TODO Auto-generated method stub
		if (work.getHour() < 18&&work.getHour()>=14) {
			System.out.println("下午的状态还不错。");
		} else {
			work.setState(new NightState());
			work.coding();
		}
	}

}
