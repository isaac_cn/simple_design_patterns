package me.isaac.simple_design_patterns.c16_state_pattern.states;

import me.isaac.simple_design_patterns.c16_state_pattern.State;
import me.isaac.simple_design_patterns.c16_state_pattern.Work;

/**
 * 状态模式中的ConcreteState类
 *
 * @author IsaacCn
 * @date 2015/12/8
 */
public class NoonState extends State {

	@Override
	public void handleWork(Work work) {
		// TODO Auto-generated method stub
		if (work.getHour() < 14 && work.getHour() >= 12) {	// 《大话设计模式》中只写了一个work.hour<14限定条件，这是不严谨的，
			//因为你不知道在Context类中将State实例化为了哪一个子类对象
			System.out.println("午休时间，吃吃、喝喝、睡一觉。");
		} else {
			work.setState(new AfternoonState());
			work.coding();
		}
	}

}
