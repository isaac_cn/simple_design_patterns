package me.isaac.simple_design_patterns.c16_state_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C16RunCase implements RunCaseBase {
    @Override
    public void run() {
        Work work = new Work();
        work.setHour(8);
        work.coding();
        work.setHour(10);
        work.coding();
        work.setHour(12);
        work.coding();
        work.setHour(14);
        work.coding();
        work.setHour(16);
        work.coding();
        work.setHour(18);
        work.coding();
        work.setHour(20);
        work.coding();
    }
}
