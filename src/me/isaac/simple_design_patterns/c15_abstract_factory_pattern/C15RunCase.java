package me.isaac.simple_design_patterns.c15_abstract_factory_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C15RunCase implements RunCaseBase {
    private static final String USER_DB_CLASS_NAME =
            "me.isaac.simple_design_patterns.c15_abstract_factory_pattern.UserOracle";
    private static final String DEPARTMENT_DB_CLASS_NAME =
            "me.isaac.simple_design_patterns.c15_abstract_factory_pattern.DepartmentDB2";

    @Override
    public void run() {
        Class<?> user = null;
        UserImpl userInstance = null;
        try {
            user = Class.forName(USER_DB_CLASS_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            userInstance = (UserImpl) user.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        userInstance.queryUser();
        userInstance.insertUser();

        Class<?> department = null;
        DepartmentImpl departmentInstance = null;
        try {
            department = Class.forName(DEPARTMENT_DB_CLASS_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            departmentInstance = (DepartmentImpl) department.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        departmentInstance.queryDepartment();
        departmentInstance.insetDepartment();
    }
}
