package me.isaac.simple_design_patterns.c15_abstract_factory_pattern;

public interface DepartmentImpl {
	void queryDepartment();

	void insetDepartment();
}
