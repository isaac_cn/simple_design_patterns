package me.isaac.simple_design_patterns.c15_abstract_factory_pattern;

public class UserDB2 implements UserImpl {

	@Override
	public void queryUser() {
		// TODO Auto-generated method stub
		System.out.println("对DB2数据库执行queryUser成功");
	}

	@Override
	public void insertUser() {
		// TODO Auto-generated method stub
		System.out.println("对DB2数据库执行insertUser成功");
	}
}
