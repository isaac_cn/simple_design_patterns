package me.isaac.simple_design_patterns.c15_abstract_factory_pattern;

public class DepartmentOracle implements DepartmentImpl {

	@Override
	public void queryDepartment() {
		// TODO Auto-generated method stub
		System.out.println("对Oracle数据库执行queryDepartment成功");
	}

	@Override
	public void insetDepartment() {
		// TODO Auto-generated method stub
		System.out.println("对Oracle数据库执行insertDepartment成功");
	}

}
