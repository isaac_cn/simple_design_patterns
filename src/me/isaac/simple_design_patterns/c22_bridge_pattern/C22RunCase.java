package me.isaac.simple_design_patterns.c22_bridge_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;
import me.isaac.simple_design_patterns.c22_bridge_pattern.apps.CellPhoneContact;
import me.isaac.simple_design_patterns.c22_bridge_pattern.apps.CellPhoneGame;
import me.isaac.simple_design_patterns.c22_bridge_pattern.cell_phones.CellPhone;
import me.isaac.simple_design_patterns.c22_bridge_pattern.cell_phones.NokiaCellPhone;
import me.isaac.simple_design_patterns.c22_bridge_pattern.cell_phones.SamsungCellPhone;

/**
 * Created by Isaac on 2017/2/4.
 */
public class C22RunCase implements RunCaseBase {
    @Override
    public void run() {
        // TODO Auto-generated method stub
        CellPhone phone=new NokiaCellPhone("Nokia 1100");
        phone.setApp(new CellPhoneContact());
        phone.run();
        phone.setApp(new CellPhoneGame());
        phone.run();

        phone=new SamsungCellPhone("Samsung i9000");
        phone.setApp(new CellPhoneContact());
        phone.run();
        phone.setApp(new CellPhoneGame());
        phone.run();
    }
}
