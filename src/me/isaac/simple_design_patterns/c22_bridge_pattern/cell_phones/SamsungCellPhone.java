package me.isaac.simple_design_patterns.c22_bridge_pattern.cell_phones;

import me.isaac.simple_design_patterns.c22_bridge_pattern.apps.CellPhoneApp;

public class SamsungCellPhone extends CellPhone {

	public SamsungCellPhone(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public SamsungCellPhone(String name, CellPhoneApp app) {
		super(name, app);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(getName()+"-run:");
		this.app.run();
	}

}
