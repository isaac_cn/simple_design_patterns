package me.isaac.simple_design_patterns.c22_bridge_pattern.cell_phones;

import me.isaac.simple_design_patterns.c22_bridge_pattern.apps.CellPhoneApp;

public abstract class CellPhone {
	private String name;
	protected CellPhoneApp app;

	public CellPhone(String name) {
		this.name = name;
	}

	public CellPhone(String name, CellPhoneApp app) {
		this.name = name;
		this.app = app;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CellPhoneApp getApp() {
		return app;
	}

	public void setApp(CellPhoneApp app) {
		this.app = app;
	}

	public abstract void run();
}
