package me.isaac.simple_design_patterns.c22_bridge_pattern.apps;

public abstract class CellPhoneApp {
	public abstract void run();
}
