package me.isaac.simple_design_patterns.c01_static_factory_method;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C01RunCase implements RunCaseBase{
    @Override
    public void run(){
        Operation operation;
        operation = OperationFactory.createOperation("+");
        operation.setNumberA(1);
        operation.setNumberB(2);
        System.out.println(operation.getResult());
    }
}
