package me.isaac.simple_design_patterns.c01_static_factory_method;

public class OperationFactory {
	private static Operation operation;

	public static Operation createOperation(String operate) {
		switch (operate) {
		case "+":
			operation = new OperationAdd();
			break;
		case "-":
			operation = new OperationMinus();
			break;
		case "*":
			operation = new OperationMultiply();
			break;
		case "/":
			operation = new OperationDivide();
			break;
		default:
			break;
		}
		return operation;
	}
}
