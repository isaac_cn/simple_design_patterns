package me.isaac.simple_design_patterns.c01_static_factory_method;

public class OperationMultiply extends Operation{

	@Override
	public double getResult() {
		// TODO Auto-generated method stub
		return this.getNumberA()*this.getNumberB();
	}
}
