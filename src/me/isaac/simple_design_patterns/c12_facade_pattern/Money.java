package me.isaac.simple_design_patterns.c12_facade_pattern;

public class Money {
	public void saveMoney(double num) {
		System.out.println("基金公司向第三方托管银行存入人民币" + num);
	}

	public void drawMoney(double num) {
		System.out.println("基金公司向第三方托管银行取出人民币" + num);
	}
}
