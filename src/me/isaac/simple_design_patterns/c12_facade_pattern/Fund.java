package me.isaac.simple_design_patterns.c12_facade_pattern;

public class Fund {
	private static final double MONEY_PERCENTAGE = 0.1;
	private static final double STOCK_PERCENTAGE = 0.7;
	private static final double BOND_PERCENTAGE = 0.2;
	private static final double BUY_RATE = 0.015;
	private static final double SELL_RATE = 0.005;

	private Money money;
	private Stock stock;
	private Bond bond;

	private double value;

	public Fund() {
		money = new Money();
		stock = new Stock();
		bond = new Bond();
	}

	public void buyFund(double num) {
		value = num * (1 - BUY_RATE);
		money.saveMoney(num * MONEY_PERCENTAGE);
		stock.buyStock(num * STOCK_PERCENTAGE);
		bond.buyBond(num * BOND_PERCENTAGE);
		System.out.println("[提醒]前端收费后，您所购买的基金市值为" + value);
	}

	public void valueChange(double increaseRate) {
		value = value * (1 + increaseRate);
		System.out.println("[提醒]您的股票增值" + increaseRate * 100 + "%");
	}

	public void sellAllFund() {
		value = value * (1 - SELL_RATE);
		money.drawMoney(value * MONEY_PERCENTAGE);
		stock.sellStock(value * STOCK_PERCENTAGE);
		bond.sellBond(value * BOND_PERCENTAGE);
		System.out.println("[提醒]赎回后，您共获得人民币" + value);
	}
}
