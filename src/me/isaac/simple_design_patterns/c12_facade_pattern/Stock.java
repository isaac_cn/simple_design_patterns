package me.isaac.simple_design_patterns.c12_facade_pattern;

public class Stock {
	public void buyStock(double num) {
		System.out.println("基金公司在证券交易所挂牌购买价值人民币" + num + "元的股票");
	}

	public void sellStock(double num) {
		System.out.println("基金公司在证券交易所挂牌出售价值人民币" + num + "元的股票");
	}
}
