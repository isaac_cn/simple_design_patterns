package me.isaac.simple_design_patterns.c12_facade_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C12RunCase implements RunCaseBase {
    @Override
    public void run() {
        Fund tomsFund = new Fund();
        tomsFund.buyFund(10000);
        tomsFund.valueChange(0.0523);
        tomsFund.sellAllFund();
    }
}
