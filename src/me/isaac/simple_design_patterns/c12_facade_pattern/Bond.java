package me.isaac.simple_design_patterns.c12_facade_pattern;

public class Bond {
	public void buyBond(double num) {
		System.out.println("基金公司在二级市场挂牌购买价值人民币" + num + "元的股票");
	}

	public void sellBond(double num) {
		System.out.println("基金公司在二级市场挂牌出售价值人民币" + num + "元的股票");
	}
}

