package me.isaac.simple_design_patterns.c02_strategy_pattern;

public class CashStrategyNormal extends CashStrategy {
	public double getFinalDiscount() {
		finalDiscount = 0d;
		return finalDiscount;
	}
}
