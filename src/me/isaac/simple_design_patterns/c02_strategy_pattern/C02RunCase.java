package me.isaac.simple_design_patterns.c02_strategy_pattern;

import me.isaac.simple_design_patterns.RunCaseBase;

/**
 * Created by Isaac on 2017/2/3.
 */
public class C02RunCase implements RunCaseBase {
    @Override
    public void run() {
        System.out.println(new CashContext(300d, "不打折").getFinalPayment());
        System.out.println(new CashContext(600d, "满500返200").getFinalPayment());
        System.out.println(new CashContext(300d, "打8折").getFinalPayment());
        System.out.println(new CashContext(300d, "满500返200").getFinalPayment());
        System.out.println(new CashContext(600d, "不打折").getFinalPayment());
    }
}
