package me.isaac.simple_design_patterns.c02_strategy_pattern;

public class CashStrategyRebate extends CashStrategy {
	private double finalDiscount;

	public CashStrategyRebate(double total, double rebateThreshould, double rebateNum) {
		// TODO Auto-generated constructor stub
		if (total >= rebateThreshould) {
			this.finalDiscount = rebateNum;
		} else {
			finalDiscount = 0;
		}
	}

	@Override
	public double getFinalDiscount() {
		// TODO Auto-generated method stub
		return finalDiscount;
	}

}
