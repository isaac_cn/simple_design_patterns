package me.isaac.simple_design_patterns.c02_strategy_pattern;

public abstract class CashStrategy {
	protected double finalDiscount;
	protected double total;
	public abstract double getFinalDiscount();
}
