package me.isaac.simple_design_patterns.c02_strategy_pattern;

public class CashStrategyDiscount extends CashStrategy {
	public CashStrategyDiscount(double totalValue, double discount) {
		this.finalDiscount = totalValue * (1 - discount);
	}

	@Override
	public double getFinalDiscount() {
		// TODO Auto-generated method stub
		return finalDiscount;
	}

}
