package me.isaac.simple_design_patterns.c02_strategy_pattern;

public class CashContext {
	private CashStrategy cashStrategy;
	private double finalDiscount;
	private double totalValue;

	public CashContext(Double totalValue, String discountType) {
		this.totalValue = totalValue;
		switch (discountType) {
		case "不打折":
			cashStrategy = new CashStrategyNormal();
			break;
		case "打8折":
			cashStrategy = new CashStrategyDiscount(totalValue, 0.8d);
			break;
		case "满500返200":
			cashStrategy = new CashStrategyRebate(totalValue, 500d, 200d);
			break;
		default:
			break;
		}
	}

	public double getFinalPayment() {
		return this.totalValue-cashStrategy.getFinalDiscount();
	}
}
